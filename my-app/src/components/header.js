import React from "react";
import { Link } from "react-router-dom";
import { Button, PageHeader, Layout, Menu, Typography } from "antd";

const { Title } = Typography;

function Header() {
  return (
    <div>
      <PageHeader
        className="ant-page-header"
        title="CIS2169 Coursework 2 "
        subTitle="by Kevin Fechete"
        extra={[
          <Menu className="ant-menu" mode="horizontal">
            <Menu.Item key="1">
              <Link to="/home">Home</Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/legacy">Legacy Fetch Modules</Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/degree">Degree Programmes</Link>
            </Menu.Item>
            <Menu.Item key="4">
              <Link to="/module">Modules</Link>
            </Menu.Item>
            <Menu.Item key="5">
              <Link to="/assessment">Assessments</Link>
            </Menu.Item>
          </Menu>,
        ]}
      />
    </div>
  );
}

export default Header;
