import React, { Component } from "react";
import { Collapse, Descriptions } from "antd";
import ModuleData from "../data/module-1.json";

const { Panel } = Collapse;

function callback(key) {
  console.log(key);
}
class DegreeModules extends Component {
  render() {
    return (
      <div>
        {ModuleData.map((moduleDetail, index) => {
          return (
            <Collapse onChange={callback}>
              <Panel
                header={moduleDetail.Course.CourseID}
                extra={moduleDetail.Course.CourseTitle}
                key="1"
              >
                <Descriptions title="Degree Info">
                  <Descriptions.Item label="Learning Outcomes">
                    <ul>
                      <li>{moduleDetail.Course.CourseLO}</li>
                    </ul>
                  </Descriptions.Item>
                  <Descriptions.Item label="Exit Awards">
                    {moduleDetail.Course.ExitAwards}
                  </Descriptions.Item>
                </Descriptions>
              </Panel>
            </Collapse>
          );
        })}
      </div>
    );
  }
}

export default DegreeModules;
