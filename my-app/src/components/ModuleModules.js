import React, { Component } from "react";
import { Collapse, Descriptions } from "antd";
import ModuleData from "../data/module-1.json";

const { Panel } = Collapse;

function callback(key) {
  console.log(key);
}
class ModuleModules extends Component {
  render() {
    return (
      <div>
        {ModuleData.map((moduleDetail, index) => {
          return (
            <Collapse onChange={callback}>
              <Panel
                header={moduleDetail.Module.ModuleID}
                extra={moduleDetail.Module.Title}
                key="1"
              >
                <Descriptions title="Module Info">
                  <Descriptions.Item label="Learning Outcomes">
                    <ul>
                      <li>{moduleDetail.Module.ModuleLO}</li>
                    </ul>
                  </Descriptions.Item>
                  <Descriptions.Item label="Exit Awards">
                    <ul>
                      <li>{moduleDetail.Module.Hours}</li>
                    </ul>
                  </Descriptions.Item>
                  <Descriptions.Item label="Exit Awards">
                    <ul>
                      <li>{moduleDetail.Module.Credits}</li>
                    </ul>
                  </Descriptions.Item>
                </Descriptions>
              </Panel>
            </Collapse>
          );
        })}
      </div>
    );
  }
}

export default ModuleModules;
