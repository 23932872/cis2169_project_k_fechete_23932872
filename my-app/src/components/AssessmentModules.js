import React, { Component } from "react";
import { Collapse, Descriptions } from "antd";
import ModuleData from "../data/module-1.json";

const { Panel } = Collapse;

function callback(key) {
  console.log(key);
}
class AssessmentModules extends Component {
  render() {
    return (
      <div>
        {ModuleData.map((moduleDetail, index) => {
          return (
            <Collapse onChange={callback}>
              <Panel
                header={moduleDetail.Assessment.AssessmentID}
                extra={moduleDetail.Assessment.Title}
                key="1"
              >
                <Descriptions title="Assessment Info">
                  <Descriptions.Item label="Learning Outcomes">
                    <ul>
                      <li>{moduleDetail.Assessment.AssessmentLO}</li>
                    </ul>
                  </Descriptions.Item>
                  <Descriptions.Item label="Assignments">
                    <ul>
                      <li>{moduleDetail.Assessment.Assignment}</li>
                    </ul>
                  </Descriptions.Item>
                  <Descriptions.Item label="Volume">
                    <ul>
                      <li>{moduleDetail.Assessment.Volume}</li>
                    </ul>
                  </Descriptions.Item>
                </Descriptions>
              </Panel>
            </Collapse>
          );
        })}
      </div>
    );
  }
}

export default AssessmentModules;
