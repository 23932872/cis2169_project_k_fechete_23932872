import React, { Component } from "react";
import { Collapse, Descriptions } from "antd";
import ModuleData from "../data/module-1.json";

const { Panel } = Collapse;

function callback(key) {
  console.log(key);
}
class LegacyModules extends Component {
  render() {
    return (
      <div>
        {ModuleData.map((moduleDetail, index) => {
          return (
            <Collapse onChange={callback}>
              <Panel
                header={moduleDetail.Course.CourseID}
                extra={moduleDetail.Course.CourseTitle}
                key="1"
              >
                <Descriptions title="Degree Info">
                  <Descriptions.Item label="Learning Outcomes">
                    <ul>
                      <li>{moduleDetail.Course.CourseLO}</li>
                    </ul>
                  </Descriptions.Item>
                  <Descriptions.Item label="Exit Awards">
                    <ul>
                      <li>{moduleDetail.Course.ExitAwards}</li>
                    </ul>
                  </Descriptions.Item>
                </Descriptions>
                <Collapse defaultActiveKey={["1"]}>
                  <Panel
                    header={moduleDetail.ModuleID}
                    extra={moduleDetail.Title}
                    key="1"
                  >
                    <Descriptions title="Module Info">
                      <Descriptions.Item label="Learning Outcomes">
                        <ul>
                          <li>{moduleDetail.Module.ModuleLO}</li>
                        </ul>
                      </Descriptions.Item>
                      <Descriptions.Item label="Exit Awards">
                        <ul>
                          <li>{moduleDetail.Module.Hours}</li>
                        </ul>
                      </Descriptions.Item>
                      <Descriptions.Item label="Exit Awards">
                        <ul>
                          <li>{moduleDetail.Module.Credits}</li>
                        </ul>
                      </Descriptions.Item>
                    </Descriptions>
                  </Panel>
                </Collapse>
                <Collapse defaultActiveKey={["1"]}>
                  <Panel
                    header={moduleDetail.ModuleID}
                    extra={moduleDetail.Title}
                    key="1"
                  >
                    <Descriptions title="Assessment Info">
                      <Descriptions.Item label="Learning Outcomes">
                        <ul>
                          <li>{moduleDetail.Assessment.AssessmentLO}</li>
                        </ul>
                      </Descriptions.Item>
                      <Descriptions.Item label="Assigments">
                        <ul>
                          <li>{moduleDetail.Assessment.Assignment}</li>
                        </ul>
                      </Descriptions.Item>
                      <Descriptions.Item label="Volume">
                        <ul>
                          <li>{moduleDetail.Assessment.Volume}</li>
                        </ul>
                      </Descriptions.Item>
                    </Descriptions>
                  </Panel>
                </Collapse>
              </Panel>
            </Collapse>
          );
        })}
      </div>
    );
  }
}

export default LegacyModules;
