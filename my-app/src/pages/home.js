import React from "react";
import { Link } from "react-router-dom";
import {
  Button,
  PageHeader,
  Layout,
  Menu,
  Typography,
  Row,
  Col,
  Divider,
  Card,
} from "antd";

const style = {
  textAlign: "center",
  width: "100%",
  height: "100%",
};

const { Title } = Typography;

function HomePage() {
  return (
    <>
      <Divider
        orientation="left"
        style={{ color: "#333", fontWeight: "normal" }}
      ></Divider>
      <Title>Homepage</Title>
      <Row
        className="ant-row block"
        gutter={(24, 24)}
        justify="space-around"
        align="middle"
      >
        <Col span={18}>
          <Card className="ant-card" hoverable="true" style={style}>
            <Link to="/legacy">
              <Title>Legacy Feth Modules</Title>
            </Link>
          </Card>
        </Col>
        <Col span={18}>
          <Card hoverable="true" style={style}>
            <Link to="/degree">
              <Title>Degree Programmes</Title>
            </Link>
          </Card>
        </Col>
        <Col span={18}>
          <Card hoverable="true" style={style}>
            <Link to="/module">
              <Title>Modules</Title>
            </Link>
          </Card>
        </Col>
        <Col span={18}>
          <Card hoverable="true" style={style}>
            <Link to="/assessment">
              <Title>Assessments</Title>
            </Link>
          </Card>
        </Col>
      </Row>
    </>
  );
}

export default HomePage;
