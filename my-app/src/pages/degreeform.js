import React from "react";
import Form from "../components/degreeFormInput";
import { Row, Col, Divider, Button, Typography } from "antd";

const { Title } = Typography;

function DegreeForm() {
  return (
    <>
      <Divider
        orientation="left"
        style={{ color: "#333", fontWeight: "normal" }}
      ></Divider>

      <Title>Degree Programmes</Title>

      <Form />
    </>
  );
}

export default DegreeForm;
