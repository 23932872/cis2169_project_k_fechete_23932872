import React from "react";
import { Link } from "react-router-dom";
import { Row, Col, Divider, Button, Typography } from "antd";

import DegreeModule from "../components/DegreeModules";

const { Title } = Typography;

function DegreePage() {
  return (
    <>
      <Divider
        orientation="left"
        style={{ color: "#333", fontWeight: "normal" }}
      ></Divider>
      <Row gutter={20}>
        <Col className="gutter-row" span={20}>
          <div>
            <Title>Degree Programmes</Title>
          </div>
        </Col>
        <Col className="gutter-row" span={4}>
          <div>
            <Button type="primary" shape="round" size="large">
              <Link to="/degreeform">Create Degree Programme</Link>
            </Button>
          </div>
        </Col>
      </Row>

      <DegreeModule />
    </>
  );
}

export default DegreePage;
