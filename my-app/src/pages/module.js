import React from "react";
import { Link } from "react-router-dom";
import { Row, Col, Divider, Button, Typography } from "antd";
import ModuleModules from "../components/ModuleModules";

const { Title } = Typography;

function ModulePage() {
  return (
    <>
      <Divider
        orientation="left"
        style={{ color: "#333", fontWeight: "normal" }}
      ></Divider>
      <Row gutter={20}>
        <Col className="gutter-row" span={20}>
          <div>
            {" "}
            <Title>Modules</Title>
          </div>
        </Col>
        <Col className="gutter-row" span={4}>
          <div>
            <Button type="primary" shape="round" size="large">
              <Link to="/moduleform">Create Module</Link>
            </Button>
          </div>
        </Col>
      </Row>

      <ModuleModules />
    </>
  );
}

export default ModulePage;
