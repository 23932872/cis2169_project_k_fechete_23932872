import React from "react";
import Form from "../components/assessmentFormInput";
import { Divider, Typography } from "antd";

const { Title } = Typography;

function AssessmentForm() {
  return (
    <>
      <Divider
        orientation="left"
        style={{ color: "#333", fontWeight: "normal" }}
      ></Divider>

      <Title>Degree Programmes</Title>

      <Form />
    </>
  );
}

export default AssessmentForm;
