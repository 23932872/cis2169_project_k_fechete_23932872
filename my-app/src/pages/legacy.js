import React from "react";
import LegacyModules from "../components/LegacyModules";
import { Row, Col, Divider, Button, Typography } from "antd";

const { Title } = Typography;

function LegacyPage() {
  return (
    <>
      <Divider
        orientation="left"
        style={{ color: "#333", fontWeight: "normal" }}
      ></Divider>
      <Title>Legacy Fetch Modules</Title>
      <LegacyModules />
    </>
  );
}

export default LegacyPage;
