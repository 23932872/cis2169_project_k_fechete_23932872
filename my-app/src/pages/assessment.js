import React from "react";
import Form from "../components/degreeFormInput";
import { Link } from "react-router-dom";

import AssessmentModules from "../components/AssessmentModules";

import { Row, Col, Divider, Button, Typography } from "antd";

const { Title } = Typography;

function AssessmentPage() {
  return (
    <>
      <Divider
        orientation="left"
        style={{ color: "#333", fontWeight: "normal" }}
      ></Divider>
      <Row gutter={20}>
        <Col className="gutter-row" span={20}>
          <div>
            {" "}
            <Title>Assessements</Title>
          </div>
        </Col>
        <Col className="gutter-row" span={4}>
          <div>
            <Button type="primary" shape="round" size="large">
              <Link to="/assessmentform">Create Assessment</Link>
            </Button>
          </div>
        </Col>
      </Row>

      <AssessmentModules />
    </>
  );
}

export default AssessmentPage;
