import React from "react";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import HomePage from "./pages/home";
import LegacyPage from "./pages/legacy";
import DegreePage from "./pages/degree";
import ModulePage from "./pages/module";
import AssessmentPage from "./pages/assessment";

import DegreeForm from "./pages/degreeform";
import ModuleForm from "./pages/moduleform";
import AssessmentForm from "./pages/assessmentform";

import Header from "./components/header";

import { Button, PageHeader, Layout, Menu } from "antd";

import "./App.css";

function App() {
  const { Content, Footer } = Layout;

  return (
    <Router>
      <div className="App">
        <Layout className="ant-layout ">
          <Header className="ant-layout-footer">
            <Header />
          </Header>
          <Content style={{ padding: "2.5% 5%" }}>
            <div
              className="site-layout-content"
              style={{
                padding: 24,
                margin: 0,
                minHeight: 280,
              }}
            >
              <Switch>
                <Route path="/home" component={HomePage} />
                <Route path="/legacy" component={LegacyPage} />
                <Route path="/degree" component={DegreePage} />
                <Route path="/degreeform" component={DegreeForm} />
                <Route path="/moduleform" component={ModuleForm} />
                <Route path="/assessmentform" component={AssessmentForm} />
                <Route path="/module" component={ModulePage} />
                <Route path="/assessment" component={AssessmentPage} />
              </Switch>
            </div>
          </Content>
          <Footer
            className="ant-layout-footer"
            style={{ textAlign: "center" }}
          ></Footer>
        </Layout>
      </div>
    </Router>
  );
}

export default App;
